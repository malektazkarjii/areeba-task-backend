const axios = require("axios");
const Country = require("../Models/CountryInfo.js");
class CountryInfo {
  
  
  
  //Getting spercific details of a number
  async getNumberInfo(req, res) {
    let { number } = req.params;
    try {
    const response= await axios.get(`http://localhost:3001/api/microservice/check/${number}`)
    
		if(response.data.status === 203) {
		return res.send({message:response.data.message})
	  }

      const Number = await Country.findOne({ phone_number: number });
      if (!Number)
        return res.status(404).json({
          status: 404,
          success: false,
          message: `This Phone number:${number} is not found`,
        });
      return res.status(200).json({
        CountryCode: Number.country_code,
        CountryName: Number.country_name,
        OperatorName: Number.operator_name,
      });
    
    } catch (error) {
      
      console.log(error);
      res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  }

  // Getting all Countries
  async getAllPhoneInfo(req, res) {
    try {
      const countries = await Country.find();
      return res.status(200).json({
        status: 200,
        success: true,
        data: countries,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  }

//Adding country credentials
  async AddNumberInfo(req, res) {
    const { phone_number, country_code, country_name, operator_name } = req.body;
    if (!phone_number, !country_code, !country_name, !operator_name) {
      return res.status(400).json({
        status: 400,
        success: false,
        message: "All fields must be provided",
      });
    }
    const country = new Country({
      phone_number,
      country_code,
      country_name,
      operator_name
    });

    try {
      await country.save();
      return res.status(201).json({
        status: 201,
        success: true,
        message: country,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  }
//deleting country
  async deleteNumberInfo(req, res) {
    const { number } = req.body;
    try {
      const country = await Country.findById({ phone_number: number });
      if (!country) {
        return res.status(404).json({
          status: 404,
          success: false,
          message: `country with id ${number} does not exist`,
        });
      }
      await country.delete();
      return res.status(200).json({
        status: 200,
        success: true,
        message: `country credentials for this ${number} deleted successfully`,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  }


}
const COUNTRY = new CountryInfo();
module.exports = COUNTRY
