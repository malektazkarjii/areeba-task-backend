const Customer = require("../Models/Customer.js");
const axios = require("axios");
class CustomerInfo {
  //THESE FUNCTIONS CAN BE MORE EFFECIENT WITH MORE TIME INVESTMENT

  //Getting all customers
  async getAllCustomers(req, res) {
    try {
      const Customers = await Customer.find();
      return res.status(200).json({
        status: 200,
        success: true,
        data: Customers,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  }

  // Creating new Customer
  async AddCustomer(req, res) {
    const { customer_name, customer_address, customer_number } = req.body;
    if (!customer_name || !customer_address || !customer_number) {
      return res.status(400).json({
        status: 400,
        success: false,
        message: "All fields must be provided",
      });
    }
    const response = await axios.get(`http://localhost:3001/api/microservice/check/${customer_number}`)
    if (response.data.status === 203) {
      return res.status(203).json({
        status: 203,
        message: response.data.message
      })
    }
    const customer = new Customer({
      customer_name,
      customer_address,
      customer_number
    });

    try {
      await customer.save();
      return res.status(201).json({
        status: 201,
        success: true,
        message: customer,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }

  }
  //Updating customer
  async updateCustomer(req, res) {
    const { id } = req.params;
    const { customer_name, customer_address, customer_number } = req.body;

    if (!customer_name || !customer_address || !customer_number) {
      return res.status(400).json({
        status: 400,
        success: false,
        message: "All fields must be provided",
      });
    }
    const response = await axios.get(`http://localhost:3001/api/microservice/check/${customer_number}`)
    if (response.data.status === 203) {
      return res.status(203).json({
        status: 203,
        message: response.data.message
      })
    }

    try {
      const customer = await Customer.findById(id);
      if (!customer) {
        return res.status(404).json({
          status: 404,
          success: false,
          message: `customer with id ${id} does not exist`,
        });
      }
      const data = await customer.updateOne({
        customer_name,
        customer_address,
        customer_number
      });
      if (data)
        return res.status(200).json({
          status: 200,
          success: true,
          message: `customer with id ${id} updated successfully`,
        });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  }
  //deleting customer
  async deleteCustomer(req, res) {
    const { id } = req.params;
    try {
      const customer = await Customer.findById(id);
      if (!customer) {
        return res.status(404).json({
          status: 404,
          success: false,
          message: `customer with id ${id} does not exist`,
        });
      }
      await customer.delete();
      return res.status(200).json({
        status: 200,
        success: true,
        message: `customer with ${id} deleted successfully`,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: 500,
        success: false,
        message: error.message,
      });
    }
  }
}
const CUSTOMER = new CustomerInfo();
module.exports = CUSTOMER;