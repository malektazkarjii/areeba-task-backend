/**
* @jest-environment node
*/
const request = require('supertest');
const app = require('../app.js');

//Validation scenarios scenarios
describe("Check number validation", () => {
  test("test should have status 200 -success-", async () => {
    const response = await request(app).get("/api/country/check/71506485")
    expect(response.status).toBe(200)
  })

  test("test should have status 203 -unvalid number-", async () => {
    const response = await request(app).get("/api/country/check/71506485555555555")
    expect(response.status).toBe(203)
  })

  test("test should have status 203 -unvalid number-", async () => {
    const response = await request(app).get("/api/country/check/lllllll")
    expect(response.status).toBe(203)
  })

})
