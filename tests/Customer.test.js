/**
* @jest-environment node
*/

const request = require('supertest');
const app = require('../app.js');
//GET ALL Customers scenarios
describe("Get customers", () => {
  test("test should have status 200", async () => {
    const response = await request(app).get("/api/customer")
    expect(response.status).toBe(200)
  })
  test("test should have status 500", async () => {
    const response = await request(app).get("/api/cusmer")
    expect(response.status).toBe(404)
  })
})


//POST Customer scenarios
describe("Post customer", () => {
  test("test should have status 200", async () => {
    const response = await request(app).post("/api/customer/create")
      .send({
        "customer_name": "malekso",
        "customer_address": "bchamun",
        "customer_number": 71506458
      })
    expect(response.status).toBe(201)
  })

  test("Should specify json in the content type header", async () => {
    const response = await request(app).post("/api/customer/create")

    expect(response.headers['content-type']).toEqual(expect.stringContaining("json"))
  })

  test("test should have status 203 -unvalid number-", async () => {
    const response = await request(app).post("/api/customer/create")
      .send({
        customer_name: "malekso",
        customer_address: "bchamun",
        customer_number: 71506458888888
      })
    expect(response.status).toBe(203)
  })

  test("test should have status 400 -All fields are required-", async () => {
    const response = await request(app).post("/api/customer/create")
      .send({})
    expect(response.status).toBe(400)
  })
})

//Update Customer scenarios

describe("UPDATE customer", () => {
  test("test should have status 400 -all fields requird-", async () => {
    const response = await request(app).put("/api/customer/update/635af23379d4dce7adeca0ba")
      .send({})
    expect(response.status).toBe(400)
  })

  test("Should specify json in the content type header", async () => {
    const response = await request(app).put("/api/customer/update/635af23379d4dce7adeca0ba")
    expect(response.headers['content-type']).toEqual(expect.stringContaining("json"))
  })

  test("Should have status code with 203 -unvalid number-", async () => {
    const response = await request(app).put("/api/customer/update/635af23379d4dce7adeca0ba")
      .send({
        "customer_name": "malekso",
        "customer_address": "bchamun",
        "customer_number": "715064588888888888"
      })
    expect(response.status).toBe(203)
  })

  test("Should have status code with 404 -wrong id-", async () => {
    const response = await request(app).put("/api/customer/update/635af4d779dccce7adeca12b")
      .send({
        "customer_name": "malekso",
        "customer_address": "bchamun",
        "customer_number": 71506458
      })
    expect(response.status).toBe(404)
  })

  test("Should have status code with 200 -success-", async () => {
    const response = await request(app).put("/api/customer/update/635af23379d4dce7adeca0ba")
      .send({
        "customer_name": "malekso",
        "customer_address": "bchamun",
        "customer_number": 71506458
      })
    expect(response.status).toBe(200)
  })

})


//DELETE CUSTOMER scenarios

describe("DELETE customer", () => {

  test("Should have status code with 404 -uvalid id-", async () => {
    const response = await request(app).delete("/api/customer/delete/635af4d779d4dce7adeca12b")
    expect(response.status).toBe(404)
  })

  test("Should have status code with 200 -deleted successfully-", async () => {
    const response = await request(app).delete("/api/customer/delete/635bee1904248306f6e4c7d5")
    expect(response.status).toBe(200)
  })


})
