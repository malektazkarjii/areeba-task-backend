const express = require("express");
const CUSTOMER = require("../Controllers/CustomerControllers.js");
const router = express.Router();


router.get("/", CUSTOMER.getAllCustomers);
router.put("/update/:id", CUSTOMER.updateCustomer);
router.post("/create", CUSTOMER.AddCustomer);
router.delete("/delete/:id", CUSTOMER.deleteCustomer);

module.exports = router;