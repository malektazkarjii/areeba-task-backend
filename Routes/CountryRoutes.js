const express = require("express");
const COUNTRY = require("../Controllers/CountryinfoControllers.js")
const router = express.Router();
const PhoneValidation = require('../Microservice/Phonevalidation.js')

router.get("/check/:number",PhoneValidation);
router.get("/:number", COUNTRY.getNumberInfo);
router.get("/", COUNTRY.getAllPhoneInfo);
router.post("/create", COUNTRY.AddNumberInfo);
router.delete("/delete", COUNTRY.deleteNumberInfo);

module.exports = router;