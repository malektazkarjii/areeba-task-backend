const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const customer = require('./Routes/CustomerRoutes.js');
const dotenv = require("dotenv");
const country = require("./Routes/CountryRoutes.js");
const microservice = require("./Routes/microserviceRoute.js");

dotenv.config();
const app = express();
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//MONGO DB CONNECTION WITH THE VIA MONGOOSE
mongoose.connect(process.env.MONGO_URL, (err, connect) => {
  if (err) console.log(`Error: ${err.message}`);
  //   else {
  //     console.log(`MongoDB connected:${connect.host}`);
  //   }
});

//LINKING ROUTES TO THIS APP AND DEFINING START POINTS FOR ALL HTTP REQUESTS
app.use("/api/customer", customer);
app.use("/api/country", country);
app.use("/api/microservice", microservice);





module.exports = app;