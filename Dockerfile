FROM playground-web-backend

RUN npm install -g nodemon
WORKDIR /app/areeba-task-backend

COPY ./package.json .

RUN npm install

COPY . .

EXPOSE 80

CMD ["npm","start"]