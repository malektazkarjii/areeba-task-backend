const mongoose = require("mongoose");

const CustomerSchema = new mongoose.Schema(
  {
    customer_name: {
      type: String,
      required: [true, "Please add your name"],
      trim: true,
    },
    customer_address: {
      type: String,
      required: [true, "Please add your address"],
    },
    customer_number: {
      type: Number,
      required: [true, "Please add your number"],
      trim: true,
    },
  },
  { timestamps: true }
);

const Customer = mongoose.model("customer", CustomerSchema);

module.exports=Customer;
