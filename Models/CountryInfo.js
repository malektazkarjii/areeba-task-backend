const mongoose = require("mongoose");

const CountrySchema = new mongoose.Schema(
  {
    phone_number: {
      type: Number,
      required: [true, "Please add Phone number"],
      trim: true,
    },
    country_code: {
      type: String,
      required: [true, "Please add Country code"],
    },
    country_name: {
      type: String,
      required: [true, "Please add Country name"],
      trim: true,
    },
    operator_name: {
      type: String,
      required: [true, "Please add Operator name"],
      trim: true,
    },
  },
  { timestamps: true }
);

const Country = mongoose.model("country", CountrySchema);

module.exports = Country;