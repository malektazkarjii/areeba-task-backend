const dotenv = require('dotenv');
const app =require('./app.js');
dotenv.config();


// POSTMAN DOCUMENTATION
// https://documenter.getpostman.com/view/20664276/2s8YK4s7bm


//lISTENING TO POSRT
const PORT = process.env.PORT || 6600;
app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});


//ANY NOT FOUND ROUTE OR ENDPOINT WILL HAVE THIS MESSAGE
app.use("*", (req, res) => {
  res.status(404).json({
    status: 404,
    message: "Page Not Found! Please enter a valid URL to proceed",
  });
});
